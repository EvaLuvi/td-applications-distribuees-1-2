package com.telecom.domainmodel;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity

public class Developer {

    //attributs de la classe

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    private String firstname;
    private String lastname;
    private String password;
    private String email;
    private LocalDate startContract;

    //constructeur de la classe

    public Developer(String firstname, String lastname, String password, String email, LocalDate startContract) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.email = email;
        this.startContract = startContract;
        this.Tasks = new ArrayList<>();
    }


    @ManyToMany(mappedBy = "developers",fetch = FetchType.EAGER)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Task> Tasks;

    public void addTask(Task task) {
        this.Tasks.add(task);
    }

    // getters pour les attributs de la classe Developer
    public long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getStartContract() {
        return startContract;
    }

    public List<Task> getTasks() {
        return Tasks;
    }

    // setters pour les attributs de la classe Developer
    public void setId(long id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setStartContract(LocalDate startContract) {
        this.startContract = startContract;
    }

    public void setTasks(List<Task> tasks) {
        Tasks = tasks;
    }

}

