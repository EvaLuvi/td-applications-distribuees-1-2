package com.telecom.domainmodel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Getter
@Setter
public class ChangeLog {
    public @Id @GeneratedValue long id;
    private LocalDate occured;

    @ManyToOne
    @ToString.Exclude
    private Task task;

    @ManyToOne
    private TaskStatus sourceStatus;

    @ManyToOne
    private TaskStatus targetStatus;

    public ChangeLog(long id, LocalDate occured, Task task, TaskStatus sourceStatus, TaskStatus targetStatus) {
        this.id = id;
        this.occured = occured;
        this.task = task;
        this.sourceStatus = sourceStatus;
        this.targetStatus = targetStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getOccured() {
        return occured;
    }

    public void setOccured(LocalDate occured) {
        this.occured = occured;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public TaskStatus getSourceStatus() {
        return sourceStatus;
    }

    public void setSourceStatus(TaskStatus sourceStatus) {
        this.sourceStatus = sourceStatus;
    }

    public TaskStatus getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(TaskStatus targetStatus) {
        this.targetStatus = targetStatus;
    }
}
