package com.telecom.domainmodel;
import javax.persistence.Entity;
import javax.persistence.*;
import lombok.Data;

@Data

@Entity
public class TaskType {
    @Id
    @GeneratedValue
    public Long id;
    public String label;


    public TaskType(long id, String label) {
        this.label = label;
        this.id=id;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TaskType)) {
            return false;
        } else {
            TaskType status = (TaskType) obj;
            return this.label.equals(status.label);
        }
    }
}
