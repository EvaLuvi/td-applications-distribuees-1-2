package com.telecom.service;

import com.telecom.domainmodel.Task;

import java.util.Collection;

public interface TaskService {

    public Collection<Task> findAllTasks();
    public Task findTask(long id);
    public Task moveRightTask(Task task);
    public Task moveLeftTask(Task task);
}