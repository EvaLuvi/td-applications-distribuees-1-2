package com.telecom.service.implement;

import com.telecom.domainmodel.Task;
import com.telecom.domainmodel.TaskStatus;
import com.telecom.repository.TaskRepository;
import com.telecom.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Optional;

public class TaskServiceImplement implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Collection<Task> findAllTasks()
    {
        return taskRepository.findAll();
    }
    @Override
    public Task findTask(long id)
    {
        Optional<Task> result = taskRepository.findById(id);

        if (result.isPresent())
        {
            return result.get();
        }
        else
        {
            return null;
        }
    }
    @Override
    public Task moveRightTask(Task task)
    {
        switch (task.getStatus().getLabel())
        {
            case "To_do" : task.setStatus(new TaskStatus(1,"Doing")); break;
            case "Doing" : task.setStatus(new TaskStatus(2,"Done")); break;
            case "Done" : task.setStatus(new TaskStatus(0,"To_do")); break;
            default: break;
        }
        return task;
    }
    @Override
    public Task moveLeftTask(Task task)
    {
        switch (task.getStatus().getLabel())
        {
            case "To_do" : task.setStatus(new TaskStatus(2,"Done")); break;
            case "Doing" : task.setStatus(new TaskStatus(0,"To_do")); break;
            case "Done" : task.setStatus(new TaskStatus(1,"Doing")); break;
            default: break;
        }
        return task;
    }
}