package com.telecom.service.implement;

import com.telecom.domainmodel.Developer;
import com.telecom.repository.DeveloperRepository;
import com.telecom.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeveloperServiceImplement implements DeveloperService {
    @Autowired
    private DeveloperRepository developerRepositoryRepo;
    @Override
    public List<Developer> findAllDeveloper()
    {
        return this.developerRepositoryRepo.findAll();
    }
}