package com.telecom.service;

import com.telecom.domainmodel.Developer;

import java.util.List;

public interface DeveloperService
{
    public List<Developer> findAllDeveloper();
}
