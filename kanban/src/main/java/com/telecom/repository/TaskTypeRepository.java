package com.telecom.repository;
import org.springframework.stereotype.Repository;
import com.telecom.domainmodel.TaskType;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository

public interface TaskTypeRepository extends JpaRepository<TaskType, Long>{
}
