package com.telecom.repository;
import org.springframework.stereotype.Repository;
import com.telecom.domainmodel.ChangeLog;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ChangeLogRepository extends JpaRepository<ChangeLog, Long>{
}
