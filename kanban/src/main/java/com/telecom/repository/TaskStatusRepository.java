package com.telecom.repository;
import org.springframework.stereotype.Repository;
import com.telecom.domainmodel.TaskStatus;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository

public interface TaskStatusRepository extends JpaRepository<TaskStatus, Long>{
}
