package com.telecom.repository;
import com.telecom.domainmodel.Developer;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface DeveloperRepository extends JpaRepository<Developer, Long>{
}
