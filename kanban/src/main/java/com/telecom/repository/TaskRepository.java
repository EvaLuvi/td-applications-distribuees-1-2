package com.telecom.repository;
import org.springframework.stereotype.Repository;
import com.telecom.domainmodel.Task;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository

public interface TaskRepository extends JpaRepository<Task, Long>{
}
