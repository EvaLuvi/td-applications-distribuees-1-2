package com.telecom;

import com.telecom.domainmodel.*;
import com.telecom.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.time.LocalDate;

@Component
public class CmdRunner implements CommandLineRunner{

    private final Logger logger = LoggerFactory.getLogger(CmdRunner.class);
    @Autowired
    ChangeLogRepository changeLogRepository;
    @Autowired
    DeveloperRepository developerRepository;
    @Autowired
    TaskRepository taskRepository;
    @Autowired
    TaskStatusRepository taskStatusRepository;
    @Autowired
    TaskTypeRepository taskTypeRepository;
    @Override
    public void run (String... args) throws Exception
    {
        logger.info("Chargement des données");
        LocalDate now = LocalDate.now();

        //déclaration des développeurs
        Developer developer1;
        developer1 = new Developer("Ruben", "Felicitations", "motdepasse1", "addresse1@gmail.com", LocalDate.of(1990, 9, 24));
        Developer developer2;
        developer2 = new Developer("Mathieu", "Chemin", "motdepasse2", "addresse2@gmail.com", LocalDate.of(1600, 9, 24));
        Developer developer3;
        developer3 = new Developer("Martin", "Pecheur", "motdepasse3", "addresse3@gmail.com", LocalDate.of(1890, 9, 24));
        Developer developer4;
        developer4 = new Developer("Martine", "Pecheur", "motdepasse4", "addresse4@gmail.com", LocalDate.of(1492, 9, 24));

        //création des taches
        TaskStatus doing = new TaskStatus(1,"Doing");
        TaskStatus to_do = new TaskStatus(0,"To_do");
        TaskStatus done = new TaskStatus(2,"Done");

        TaskType type1;
        type1 = new TaskType(0,"Travail");
        TaskType type2;
        type2 = new TaskType(1,"Bugs");

        Task task1;
        task1 = new Task(0,"Task 1", 10, 11, now, type1, done);
        Task task2;
        task2 = new Task(1,"Task 2", 4, 6, now, type2, done);

        //assignation des taches
        task1.addDeveloper(developer1);
        task1.addDeveloper(developer3);
        task2.addDeveloper(developer2);
        task1.addDeveloper(developer4);

        ChangeLog changeLog1 = new ChangeLog(0, now, task1, doing, done);
        ChangeLog changeLog2 = new ChangeLog(1, now, task2, to_do, doing);

        // Enregistrement des données
        changeLogRepository.save(changeLog1);
        changeLogRepository.save(changeLog2);

        developerRepository.save(developer1);
        developerRepository.save(developer2);
        developerRepository.save(developer3);

        taskStatusRepository.save(to_do);
        taskStatusRepository.save(doing);
        taskStatusRepository.save(done);

        taskTypeRepository.save(type1);
        taskTypeRepository.save(type2);

        taskRepository.save(task1);
        taskRepository.save(task2);
    }
}
